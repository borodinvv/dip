FROM python:3.9
MAINTAINER vladislav_borodin@epam.com
RUN useradd -ms /bin/bash weather
USER weather
WORKDIR /weather
COPY --chown=weather:weather . .
ENV PATH="/home/weather/.local/bin:${PATH}"
RUN pip3 install --user -r requirements.txt
EXPOSE 80
CMD ["python3", "-m" , "flask", "run", "--host=0.0.0.0", "--port=80"]
