# -*- coding: utf-8 -*-

import ssl
import urllib.request
import time
import requests
import json
import calendar
import mysql.connector
from datetime import date, datetime, timedelta
from flask import Flask, request, render_template, jsonify
from joblib import Parallel, delayed


def worker(i):
    print('worker ', i)
    x = 0
    while x < 1000:
        print(x)
        x += 1

def weather():
    ssl._create_default_https_context = ssl._create_unverified_context
    
    BaseURL = 'https://www.metaweather.com/api/location/2122265/'
    
    
    URL = BaseURL
    
    print(' - Running query URL: ', URL)
    print()
    
    
    response = urllib.request.urlopen(URL)
    data = response.read()
    weatherData = json.loads(data.decode('utf-8'))
    
    errorCode=weatherData["errorCode"] if 'errorCode' in weatherData else 0
    
    if (errorCode>0):
        print("An error occurred retrieving the data:"+weatherData["message"])
        exit("Script terminated")
        
    print( "Connecting to mysql database")
    
    cnx = mysql.connector.connect(host='weatherappdb.cedoqjgahqyr.us-east-2.rds.amazonaws.com',
        user='weather',
        passwd='DB_PASS',
        database='weather')
    
    cursor = cnx.cursor()
    
    
    add_data_weather=("CREATE TABLE IF NOT EXISTS `weather`.`data_weather` (`id` varchar(32) DEFAULT NULL,`weather_state_name` varchar(32) DEFAULT NULL,`wind_direction_compass` varchar(32) DEFAULT NULL,`created` datetime DEFAULT NULL,`applicable_date` datetime DEFAULT NULL,`the_temp` float DEFAULT NULL,`max_temp` float DEFAULT NULL,`min_temp` float DEFAULT NULL)" 
                    "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;")
    cursor.execute(add_data_weather)
    cnx.commit()
    
    delete_data_weather=("TRUNCATE TABLE `weather`.`data_weather`")
    cursor.execute(delete_data_weather)
    cnx.commit()
    
    
    insert_data_weather = ("INSERT INTO `weather`.`data_weather`"
                    "(`id`,`weather_state_name`,`wind_direction_compass`,`created`,`applicable_date`,`min_temp`,`max_temp`,`the_temp`)"
                    "VALUES (%(id)s, %(weather_state_name)s, %(wind_direction_compass)s, %(created)s, %(applicable_date)s, %(min_temp)s, %(max_temp)s, %(the_temp)s)")
    
    
    c_w = weatherData["consolidated_weather"]
    for value in c_w:
        data_wx = {
        'id': value["id"],
        'weather_state_name': value["weather_state_name"],
        'wind_direction_compass': value["wind_direction_compass"],
        'created': value["created"],
        'applicable_date': value["applicable_date"],
        'the_temp': value["the_temp"],
        'max_temp': value["max_temp"],
        'min_temp': value["min_temp"]
        }
        cursor.execute(insert_data_weather, data_wx)
        cnx.commit()
    
    
                   
    cursor.close() 
    cnx.close()
    print( "Database connection closed")
    

app = Flask(__name__)

@app.route('/')
def index():
    weather()
    cnx = mysql.connector.connect(host='weatherappdb.cedoqjgahqyr.us-east-2.rds.amazonaws.com',
    user='weather',
    passwd='DB_PASS',
    database='weather')
    cursor = cnx.cursor()
    cursor.execute("SELECT * FROM data_weathe")
    data = cursor.fetchall()
    return render_template('index.html', data=data)
    print(data)
    cursor.close() 
    cnx.close()

@app.route('/update', methods=['POST','GET'])
def update():
    weather()
    cnx = mysql.connector.connect(host='weatherappdb.cedoqjgahqyr.us-east-2.rds.amazonaws.com',
    user='weather',
    passwd='DB_PASS',
    database='weather')
    cursor = cnx.cursor()
    cursor.execute("SELECT * FROM data_weather")
    data = cursor.fetchall()
    return render_template('update.html', data=data)
    print(data)
    cursor.close() 
    cnx.close()

# @app.route('/results', methods=['POST','GET'])
# def results():
#     select = request.form.get('date_select')
#     weather()
#     cnx = mysql.connector.connect(host='weatherappdb.cedoqjgahqyr.us-east-2.rds.amazonaws.com',
#     user='weather',
#     passwd='DB_PASS',
#     database='weather')
#     cursor = cnx.cursor()
#     cursor.execute(""" SELECT * FROM data_weather WHERE applicable_date = '{}' ORDER BY created; """.format(select))
#     data = cursor.fetchall()
#     return render_template('results.html', select=select, data=data)
#     print(data)
#     cursor.close() 
#     cnx.close()

@app.route('/stress')
def stress():
    start_time = time.time()
    Parallel(n_jobs=-1, prefer="processes", verbose=0)(
            delayed(worker)(num)
            for num in range(12000)
    )
    end_time = time.time() - start_time
    resp = jsonify(success=True, time=str(end_time))
    resp.status_code = 200
    return resp

print( "Flask is started")
print( "Done")

if __name__ == '__main__':
    app.run()